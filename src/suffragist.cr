require "kemal"
require "ecr"

Choices = {
  "HAM" => "Hamburger",
  "PIZ" => "Pizza",
  "CUR" => "Curry",
  "NOO" => "Noodles",
}

macro my_renderer(filename)
  render "src/views/#{ {{filename}} }.ecr", "src/views/layout.ecr"
end

get "/" do
  title = "Welcome to the Suffragist!"
  my_renderer "index"
end

post "/cast" do |env|
  title = "Thanks for casting your vote!"
  vote  = env.params.body["vote"].as(String)
  my_renderer "cast"
end

get "/results" do
  title = "Results"
  votes = { "HAM" => 7, "PIZ" => 5, "CUR" => 3 }
  my_renderer "results"
end

Kemal.run
